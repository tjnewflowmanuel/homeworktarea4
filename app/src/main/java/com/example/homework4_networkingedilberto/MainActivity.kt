package com.example.homework4_networkingedilberto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val  message= "EDILBERTO MANUEL QUISPE FLORES"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    private fun showMessage(message:String){
        Toast.makeText(this, message,Toast.LENGTH_LONG).show()
    }
}
