package com.example.homework4_networkingedilberto.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class UserEntity(val id:String?,val username:String?,val firstname:String?,
                      val lastname:String?):Serializable

data class NoteEntity(val objectId:String?, @SerializedName("name")val title:String?,
                      val description:String?):Serializable