package com.example.homework4_networkingedilberto.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.homework4_networkingedilberto.R
import com.example.homework4_networkingedilberto.model.NoteEntity
import com.example.homework4_networkingedilberto.storage.*
import com.example.homework4_networkingedilberto.ui.dialog.EncuestaDialogFragment
import kotlinx.android.synthetic.main.activity_edit_encuesta.*
import kotlinx.android.synthetic.main.layout_loading.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditEncuestaActivity : AppCompatActivity(), EncuestaDialogFragment.DialogListener {

    private lateinit var encuestaRepository: EncuestaRepository
    private var call: Call<NoteResponse>?=null

    private var note: NoteEntity?=null

    private var name:String?=null
    private var desc:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_encuesta)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        setupRepository()
        populate()

        ui()
    }

    private fun setupRepository(){
        encuestaRepository= EncuestaRepository()
    }

    private fun ui(){
        btnEditEncuesta.setOnClickListener {
            if(validateForm()){
                editNote()
            }
        }

        btnDeleteEncuesta.setOnClickListener {
            showNoteDialog()
        }
    }

    private fun deleteNote(mNote:NoteEntity){
        val map:MutableMap<String,String> = mutableMapOf<String,String>()
        val token= PreferencesHelper.session(this)
        token?.let {
            map["user-token"] =it
        }

        call= EncuestaApiClient.build()?.deleteNote(EncuestaConstant.APPLICATION_ID, EncuestaConstant.REST_API_KEY,map,mNote.objectId)

        call?.enqueue(object : Callback<NoteResponse> {
            override fun onFailure(call: Call<NoteResponse>, t: Throwable) {
                showErrorMessage(t.message)
            }

            override fun onResponse(call: Call<NoteResponse>, response: Response<NoteResponse>) {
                response?.body()?.let {
                    if(response.isSuccessful){
                        finish()
                    }else{
                        showErrorMessage(response.errorBody()?.string())
                    }
                }
            }
        })
    }

    private fun editNote(){
       showLoading()
       val noteId= note?.objectId
       val raw= NoteRaw(name,desc)
       val map:MutableMap<String,String> = mutableMapOf<String,String>()
       val token= PreferencesHelper.session(this)
       token?.let {
            map["user-token"] =it
       }

       call= EncuestaApiClient.build()?.updateNote(EncuestaConstant.APPLICATION_ID, EncuestaConstant.REST_API_KEY,map,noteId,raw)

       call?.enqueue(object : Callback<NoteResponse> {
            override fun onFailure(call: Call<NoteResponse>, t: Throwable) {
                hideLoading()
                showErrorMessage(t.message)
            }

            override fun onResponse(call: Call<NoteResponse>, response: Response<NoteResponse>) {
                hideLoading()
                response?.body()?.let {
                    if(response.isSuccessful){
                        finish()
                    }else{
                        showErrorMessage(response.errorBody()?.string())
                    }
                }
            }
        })

        //noteRepository.updateNote(nNote)
        //finish()
    }

    override fun onPause() {
        super.onPause()
        call?.cancel()
    }


    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

    private fun validateForm():Boolean{
        name= eteName.text.toString()
        desc= eteDesc.text.toString()

        if(name.isNullOrEmpty()){
            return false
        }

        if(desc.isNullOrEmpty()){
            return false
        }

        return true
    }

    private fun populate(){
        note?.let {
            eteName.setText(it?.title)
            eteDesc.setText(it?.description)
        }
    }

    private fun showNoteDialog(){
        val noteDialogFragment= EncuestaDialogFragment()
        val bundle= Bundle()
        bundle.putString("TITLE","¿Deseas eliminar esta Encuesta?")
        bundle.putInt("TYPE",100)

        noteDialogFragment.arguments= bundle
        noteDialogFragment.show(supportFragmentManager,"dialog")
    }

    override fun onPositiveListener(any: Any?, type: Int) {
        note?.let {
            //noteRepository?.deleteNote(it)
            deleteNote(it)
        }
        //finish()
    }

    override fun onNegativeListener(any: Any?, type: Int) {}

    private fun verifyExtras(){
        intent?.extras?.let {
            note= it.getSerializable("NOTE") as NoteEntity
        }
    }

    private fun showLoading() {
        flayLoading.visibility= View.VISIBLE
    }

    private fun hideLoading() {
        flayLoading.visibility= View.GONE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
