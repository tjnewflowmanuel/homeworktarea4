package com.example.homework4_networkingedilberto.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_encuesta_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.homework4_networkingedilberto.R
import com.example.homework4_networkingedilberto.model.NoteEntity
import com.example.homework4_networkingedilberto.storage.EncuestaApiClient
import com.example.homework4_networkingedilberto.storage.EncuestaConstant
import com.example.homework4_networkingedilberto.storage.EncuestaRepository
import com.example.homework4_networkingedilberto.storage.PreferencesHelper
import com.example.homework4_networkingedilberto.ui.adapter.EncuestaAdapter
import kotlinx.android.synthetic.main.layout_loading.*


class EncuestaListActivity : AppCompatActivity() {

    private lateinit var encuestaRepository: EncuestaRepository
    private var call:Call<List<NoteEntity>>?=null

    private var notes:List<NoteEntity>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encuesta_list)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setupRepository()
        ui()
    }

    private fun ui(){
        btnAddEncuesta.setOnClickListener {
            goToAddNote()
        }

        lstEncuestas.setOnItemClickListener { parent, view, position, id ->
            notes?.let {
                if(it.isNotEmpty()){
                    val note:NoteEntity= it[position]
                    goToNote(note)
                }
            }
        }

        swipeRefresh.setOnRefreshListener {
            loadNotes()
            swipeRefresh.isRefreshing = false
        }
    }

    private fun setupRepository(){
        encuestaRepository= EncuestaRepository()
    }

    override fun onResume() {
        super.onResume()
        loadNotes()
    }

    private fun loadNotes(){
        val map:MutableMap<String,String> = mutableMapOf<String,String>()
        val token= PreferencesHelper.session(this)
        token?.let {
            map["user-token"] =it
        }

        showLoading()
        call= EncuestaApiClient.build()?.notes(EncuestaConstant.APPLICATION_ID,EncuestaConstant.REST_API_KEY,map)

        call?.enqueue(object :Callback<List<NoteEntity>>{
            override fun onFailure(call: Call<List<NoteEntity>>, t: Throwable) {
                hideLoading()
                showErrorMessage(t.message)
            }

            override fun onResponse(call: Call<List<NoteEntity>>, response: Response<List<NoteEntity>>) {
                hideLoading()
                response?.body()?.let {
                    if(response.isSuccessful){ //200
                        renderNotes(it)
                    }else{
                        showErrorMessage(response.errorBody()?.string())
                    }
                }
            }
        })
    }

    private fun renderNotes(mNotes:List<NoteEntity>?){
        notes= mNotes
        notes?.let {
            lstEncuestas.adapter= EncuestaAdapter(this,it)
        }
    }

    override fun onPause() {
        super.onPause()
        call?.cancel()
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

    private fun goToAddNote(){
        startActivity(Intent(this, AddEncuestaActivity::class.java))
    }

    private fun goToNote(note:NoteEntity){
        val bundle= Bundle()
        bundle.putSerializable("NOTE",note)
        val intent= Intent(this, EditEncuestaActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }


    private fun showLoading() {
        flayLoading.visibility=View.VISIBLE
    }

    private fun hideLoading() {
        flayLoading.visibility=View.GONE
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
