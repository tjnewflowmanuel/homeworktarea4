package com.example.homework4_networkingedilberto.storage

data class LogInRaw(val login:String?,val password:String?)

data class NoteRaw(val name:String?, val description:String?)